import Vue from "vue";
import Router from "vue-router";
import goTo from "vuetify/src/services/goto";
import routes from "./routes";

Vue.use(Router);

// @ts-ignore
const router = new Router({
  mode: process.env.pages === "true" ? "hash" : "history",
  base: "/",
  scrollBehavior: (to, from, savedPosition) => {
    if (to.hash) {
      return goTo(to.hash);
    } else if (savedPosition) {
      return goTo(savedPosition.y);
    } else {
      return goTo(0);
    }
  },
  routes
});

export default router;
