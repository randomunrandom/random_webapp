const presets = [
  [
    "@babel/preset-env",
    {
      useBuiltIns: false
    }
  ]
];

const plugins = [];

module.exports = { presets, plugins };
